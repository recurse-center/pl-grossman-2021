## Programming Languages Course Wiki

Welcome to our Wiki! 

This Wiki is for the study group at RC for Dan Grossman's Coursera course [Programming Languages](https://www.coursera.org/learn/programming-languages/). It's a sequence of 3 courses. Use this Wiki to document all resources and discussions. Use the [Issues](https://gitlab.com/recurse-center/pl-grossman-2021/-/issues) section to collaboratively review the homework assignments.




